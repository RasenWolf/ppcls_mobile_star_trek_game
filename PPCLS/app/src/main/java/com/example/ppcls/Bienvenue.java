package com.example.ppcls;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Bienvenue extends AppCompatActivity {
    private Button regle_deroulement;
    private Button compris;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bienvenue);
        this.regle_deroulement = (Button) findViewById(R.id.regle_deroulement);
        this.compris = (Button) findViewById(R.id.compris);

        regle_deroulement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versderoulemet = new Intent(getApplicationContext(),Deroulement.class);
                startActivity(versderoulemet);
                finish();
            }
        });

        compris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versmenuprincipal = new Intent(getApplicationContext(),MenuPrincipal.class);
                startActivity(versmenuprincipal);
                finish();

            }
        });

    }
}
