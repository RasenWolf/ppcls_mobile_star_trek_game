package com.example.ppcls;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class parametrage_partie extends AppCompatActivity {

    private Button retour;
    private  Button lancercombat;
    private Button lancercombatonline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametrage_partie);
        this.retour = (Button) findViewById(R.id.button3);
        this.lancercombat = (Button) findViewById(R.id.lancement_bot);
        this.lancercombatonline = (Button) findViewById(R.id.debut_recherche);

        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versmenu = new Intent(getApplicationContext(),MenuPrincipal.class);
                startActivity(versmenu);
                finish();
            }
        });
        lancercombat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent verscombat = new Intent(getApplicationContext(),combat.class);
                startActivity(verscombat);
                finish();
            }
        });
    }
}
