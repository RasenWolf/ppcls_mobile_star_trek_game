package com.example.ppcls;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class combat extends AppCompatActivity {

    private Button jouer;
    private Button abandon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_combat);
        this.jouer = (Button) findViewById(R.id.button4);
        this.abandon = (Button) findViewById(R.id.abandon);

        jouer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //quand nombre de manche atteinte ou parti fini
                Intent versresultat = new Intent(getApplicationContext(),resultat.class);
                startActivity(versresultat);
                finish();

            }
        });
        abandon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versresultat = new Intent(getApplicationContext(),resultat.class);
                startActivity(versresultat);
                finish();
            }
        });
    }
}
