package com.example.ppcls;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class Inscription extends AppCompatActivity {

    private Button retour;
    private Button Validation;
    private DatabaseManager databaseManager;
    private EditText lastname;
    private EditText name;
    private EditText email;
    private EditText password;
    private EditText login;
    private CheckBox radiohomme;
    private CheckBox radiofemme;
    String sexe;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        databaseManager = new DatabaseManager(this);
        this.retour = (Button) findViewById(R.id.retour);
        this.Validation = (Button) findViewById(R.id.Validation);
        this.lastname =(EditText)findViewById(R.id.lastname);
        this.name = (EditText) findViewById(R.id.name);
        this.login =(EditText) findViewById(R.id.log);
        this.password=(EditText )findViewById(R.id.mdp);
        this.email=(EditText) findViewById((R.id.mail));
        this.radiohomme=(CheckBox)findViewById(R.id.radiohomme);
        this.radiofemme=(CheckBox) findViewById(R.id.radiofemme);

        final String testlastname,testname,testlogin,testpassword,testmail;


        testlastname=lastname.getText().toString();
        testname=name.getText().toString();
        testlogin=login.getText().toString();
        testmail=email.getText().toString();
        testpassword=password.getText().toString();

        radiofemme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    radiohomme.setChecked(false);
                    sexe="Femme";

                }
            }
        });

        radiohomme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    radiofemme.setChecked(false);

                    sexe="Homme";
                }
            }
        });




        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent retourversmain = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(retourversmain);
                finish();

            }
        });

        if (testlastname.matches("") || testname.matches("") || testlogin.matches("") || testpassword.matches("") || testmail.matches(""))
        {
            Toast.makeText(this,"Vous devez renseigner tous les champs!",Toast.LENGTH_SHORT).show();
        }
        else
            if(radiofemme.isChecked() || radiohomme.isChecked())
            {
            Validation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent versbienvenue = new Intent(getApplicationContext(), Bienvenue.class);
                    databaseManager.incription_player(testlastname,testname,testlogin,testmail,testpassword,sexe);
                    startActivity(versbienvenue);

                    finish();
                }
            });
        }
            else
            {
                Toast.makeText(this,"Vous devez choisir un sexe!",Toast.LENGTH_SHORT).show();
            }

    }
}
