package com.example.ppcls;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.service.notification.NotificationListenerService;
import android.util.Log;

public class DatabaseManager extends SQLiteOpenHelper {
    private static final String DATABASE_NAME="Game.db";
    private static  final int DATABASE_VERSION=1;


    public DatabaseManager(Context context)
    {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String strSql= "create table T_Player("
                +   "   idPlayer integer primary key autoincrement,"
                +   "   lastName text not null,    "
                +   "   name text not null ,"
                +   "   login text not null unique, "
                +   "   email text not null unique,"
                +   "   password text not null unique,"
                +   "    sex text not null,"
                +    " score integer default 0"
                + ")";
        db.execSQL(strSql);
        Log.i("DATABASE","onCreate invoked");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {


    }
    public  void updatescore(String login,int score)

    {
        login = login.replace("'","''");
        String strSql="update T_Player(score)values('"+login+"',"+score+")";

        this.getWritableDatabase().execSQL(strSql);
        Log.i("DATABASE","updatescore invoked");
    }

    public void connexion(String login, String password)
    {
        login = login.replace("'","''");
        password = password.replace("'","''");
        String strSql=" select T_Player(login,password)values('"+login+"','"+password+"')";

        this.getReadableDatabase().execSQL(strSql);
        Log.i("DATABASE","connexion invoked");
    }

    public  void incription_player(String lastname,String name, String login,String email,String password,String sex)
    {
        login = login.replace("'","''");
        name = name.replace("'","''");
        lastname = lastname.replace("'","''");
        email = email.replace("'","''");
        password = password.replace("'","''");
        sex = sex.replace("'","''");
        String strSql=" insert into T_Player(login,password)values('"+lastname+"','"+name+"','"+login+"','"+email+"','"+password+"','"+sex+"')";
        this.getWritableDatabase().execSQL(strSql);
        Log.i("DATABASE","inscription invoked");



    }
}
