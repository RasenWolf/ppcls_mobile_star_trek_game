package com.example.ppcls;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MenuPrincipal extends AppCompatActivity {

    private Button jouer;
    private  Button classement;
    private Button reglement;
    private Button deconnexion;
    private ImageView parametre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menuprincipal);
        this.jouer = (Button) findViewById(R.id.jouer);
        this.classement = (Button) findViewById(R.id.jouer3);
        this.reglement = (Button) findViewById(R.id.jouer2);
        this.deconnexion = (Button) findViewById(R.id.disconnect);
        this.parametre = (ImageView) findViewById(R.id.parametre);

        jouer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versparametrepartie = new Intent(getApplicationContext(),parametrage_partie.class);
                startActivity(versparametrepartie);
                finish();
            }
        });

        classement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versclassement = new Intent(getApplicationContext(), com.example.ppcls.classement.class);
                startActivity(versclassement);
                finish();
            }
        });

        reglement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versreglement = new Intent(getApplicationContext(),Bienvenue.class);
                startActivity(versreglement);
                finish();
            }
        });

        deconnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versmain = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(versmain);
                finish();
            }
        });

        parametre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versparametre = new Intent(getApplicationContext(),parametres.class);
                startActivity(versparametre);
                finish();
            }
        });
    }
}
