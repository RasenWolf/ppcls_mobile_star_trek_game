package com.example.ppcls;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Deroulement extends AppCompatActivity {
    private Button retourbienvenue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deroulement);
        this.retourbienvenue = (Button) findViewById(R.id.retourbienvenue);

        retourbienvenue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versbienvenue = new Intent(getApplicationContext(),Bienvenue.class);
                startActivity(versbienvenue);
                finish();
            }
        });
    }
}
