package com.example.ppcls;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {


    private Button Connexion;
    private Button Inscription;
    private Button quit_game;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.Connexion = (Button) findViewById(R.id.Connexion);
        this.Inscription = (Button) findViewById(R.id.Inscription);
        this.quit_game = (Button)  findViewById(R.id.quitgame);
        Connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versconnexion = new Intent(getApplicationContext(),connexion.class);
                startActivity(versconnexion);
                finish();
            }


        });

       Inscription.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent versincription = new Intent(getApplicationContext(), com.example.ppcls.Inscription.class);
               startActivity(versincription);
               finish();
           }
       });

       quit_game.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               finish();


           }
       });
    }


}
