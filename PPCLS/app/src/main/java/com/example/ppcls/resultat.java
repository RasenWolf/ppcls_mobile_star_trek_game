package com.example.ppcls;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class resultat extends AppCompatActivity {

    private Button menu;
    private Button rejouer;
    private Button classement;
    private Button parametreparti;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultat);
        this.menu = (Button) findViewById(R.id.menuprincipal);
        this.rejouer= (Button) findViewById(R.id.regame);
        this.classement = (Button) findViewById(R.id.classementj);
        this.parametreparti = (Button) findViewById(R.id.parametrage);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versmenu = new Intent(getApplicationContext(),MenuPrincipal.class);
                startActivity(versmenu);
                finish();
            }
        });
        rejouer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent verscombat = new Intent(getApplicationContext(),combat.class);
                startActivity(verscombat);
                finish();
            }
        });

        classement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versclassment = new Intent(getApplicationContext(), com.example.ppcls.classement.class);
                startActivity(versclassment);
                finish();
            }
        });

        parametreparti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent versparametrageparti = new Intent(getApplicationContext(),parametrage_partie.class);
                startActivity(versparametrageparti);
                finish();
            }
        });
    }
}
