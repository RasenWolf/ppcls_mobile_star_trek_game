package com.example.ppcls;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class connexion extends AppCompatActivity {

    private Button retour;
    private  Button Connexion;
    private EditText login;
    private EditText password;
    private DatabaseManager databaseManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);
        this.retour = (Button) findViewById(R.id.retour);
        this.Connexion = (Button) findViewById(R.id.Connexion);
        this.login =(EditText) findViewById(R.id.Login);
        this.password=(EditText) findViewById(R.id.mdp) ;
         databaseManager = new DatabaseManager(this);

        final String testlogin,testpassword;

        testlogin=login.getText().toString();
        testpassword=password.getText().toString();





        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent retourversmain = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(retourversmain);
                finish();
            }
        });
        //mettre une condition si inscrit sinon affiche message erreur

        Connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent versmenuprincipal = new Intent(getApplicationContext(),MenuPrincipal.class);

                databaseManager.connexion(testlogin,testpassword);

                startActivity(versmenuprincipal);
                finish();
            }
        });



    }
}
